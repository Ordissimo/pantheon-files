# Japanese translation for pantheon-files
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd 2016
# This file is distributed under the same license as the pantheon-files package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: pantheon-files\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-24 20:43+0000\n"
"PO-Revision-Date: 2022-04-09 05:44+0000\n"
"Last-Translator: Ryo Nakano <ryonakaknock3@gmail.com>\n"
"Language-Team: Japanese <https://l10n.elementary.io/projects/files/extra/ja/>"
"\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.4.2\n"
"X-Launchpad-Export-Date: 2017-06-11 06:29+0000\n"

#: data/io.elementary.files.desktop.in.in:3
#: data/io.elementary.files.appdata.xml.in.in:8
msgid "Files"
msgstr "ファイル"

#: data/io.elementary.files.desktop.in.in:4
msgid "Browse your files"
msgstr "ファイルを閲覧します"

#: data/io.elementary.files.desktop.in.in:5
msgid "folder;manager;explore;disk;filesystem;"
msgstr ""
"folder;manager;explore;disk;filesystem;フォルダー;マネージャー;エクスプロー"
"ラー;ディスク;ファイルシステム;"

#: data/io.elementary.files.desktop.in.in:6
msgid "File Manager"
msgstr "ファイルマネージャー"

#. TRANSLATORS This string is an icon name and should not be translated.
#: data/io.elementary.files.desktop.in.in:9
msgid "system-file-manager"
msgstr "system-file-manager"

#: data/io.elementary.files.desktop.in.in:20
msgid "New Window"
msgstr "新しいウィンドウ"

#: data/io.elementary.files.desktop.in.in:24
msgid "New Window As Administrator"
msgstr "新しい管理者ウィンドウ"

#: data/io.elementary.files.appdata.xml.in.in:9
msgid "Browse and manage files and folders"
msgstr "ファイルとフォルダーを閲覧したり管理したりします"

#: data/io.elementary.files.appdata.xml.in.in:11
msgid ""
"Easily copy, move, and rename your files, or use folders to stay organized. "
"Whether you like files in lists, grids or columns, you can always find them "
"right away. Access all your files, whether locally, on an external device or "
"remotely using FTP, SFTP, AFP, Webdav, or Windows share."
msgstr ""
"ファイルのコピー・移動・名前の変更や、フォルダーを使っての整理が簡単に行えま"
"す。リスト形式、グリッド形式、カラム形式のどれが好みであっても、いつでもすぐ"
"にファイルを見つけることが可能です。ファイルの場所がローカル、外部デバイスや "
"FTP、SFTP、AFP、Webdav などのリモートや Windows 共有であっても、すべてのファ"
"イルにアクセスできます。"

#: data/io.elementary.files.appdata.xml.in.in:33
#: data/io.elementary.files.appdata.xml.in.in:51
#: data/io.elementary.files.appdata.xml.in.in:76
#: data/io.elementary.files.appdata.xml.in.in:153
#: data/io.elementary.files.appdata.xml.in.in:170
#: data/io.elementary.files.appdata.xml.in.in:194
#: data/io.elementary.files.appdata.xml.in.in:218
msgid "Improvements:"
msgstr "改善点:"

#: data/io.elementary.files.appdata.xml.in.in:35
msgid "Show New Tab and New Window shortcuts in context menus"
msgstr "コンテキストメニューに“新しいタブ”と“新しいウィンドウ”のキーボードショートカットを表示するように修正"

#: data/io.elementary.files.appdata.xml.in.in:37
#: data/io.elementary.files.appdata.xml.in.in:57
#: data/io.elementary.files.appdata.xml.in.in:85
#: data/io.elementary.files.appdata.xml.in.in:102
#: data/io.elementary.files.appdata.xml.in.in:117
#: data/io.elementary.files.appdata.xml.in.in:133
#: data/io.elementary.files.appdata.xml.in.in:148
#: data/io.elementary.files.appdata.xml.in.in:165
#: data/io.elementary.files.appdata.xml.in.in:202
#: data/io.elementary.files.appdata.xml.in.in:223
#: data/io.elementary.files.appdata.xml.in.in:243
msgid "Fixes:"
msgstr "修正点:"

#: data/io.elementary.files.appdata.xml.in.in:39
msgid "Double click selects instead of exiting while renaming in list view"
msgstr ""
"リスト表示でファイル名を変更中にファイルをダブルクリックすると、名前の変更を"
"終了するのではなくファイルを選択するように修正"

#: data/io.elementary.files.appdata.xml.in.in:40
msgid "Show public share icon in breadcrumbs"
msgstr "パンくずリストに“公開”フォルダーの共有アイコンが表示されていなかったのを修正"

#: data/io.elementary.files.appdata.xml.in.in:41
msgid "Prevent a crash when dragging to re-arrange bookmarks"
msgstr "ドラッグしてブックマークを並び替えするとクラッシュする不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:43
#: data/io.elementary.files.appdata.xml.in.in:68
#: data/io.elementary.files.appdata.xml.in.in:94
#: data/io.elementary.files.appdata.xml.in.in:258
#: data/io.elementary.files.appdata.xml.in.in:272
msgid "Minor updates:"
msgstr "そのほかのアップデート:"

#: data/io.elementary.files.appdata.xml.in.in:45
#: data/io.elementary.files.appdata.xml.in.in:70
#: data/io.elementary.files.appdata.xml.in.in:96
#: data/io.elementary.files.appdata.xml.in.in:111
#: data/io.elementary.files.appdata.xml.in.in:127
#: data/io.elementary.files.appdata.xml.in.in:142
#: data/io.elementary.files.appdata.xml.in.in:159
#: data/io.elementary.files.appdata.xml.in.in:188
#: data/io.elementary.files.appdata.xml.in.in:212
#: data/io.elementary.files.appdata.xml.in.in:230
#: data/io.elementary.files.appdata.xml.in.in:252
#: data/io.elementary.files.appdata.xml.in.in:266
#: data/io.elementary.files.appdata.xml.in.in:279
#: data/io.elementary.files.appdata.xml.in.in:292
#: data/io.elementary.files.appdata.xml.in.in:324
#: data/io.elementary.files.appdata.xml.in.in:346
#: data/io.elementary.files.appdata.xml.in.in:355
#: data/io.elementary.files.appdata.xml.in.in:364
#: data/io.elementary.files.appdata.xml.in.in:375
#: data/io.elementary.files.appdata.xml.in.in:384
#: data/io.elementary.files.appdata.xml.in.in:403
#: data/io.elementary.files.appdata.xml.in.in:414
#: data/io.elementary.files.appdata.xml.in.in:422
#: data/io.elementary.files.appdata.xml.in.in:454
msgid "Updated translations"
msgstr "翻訳の更新"

#: data/io.elementary.files.appdata.xml.in.in:53
msgid "Close FileChooser with Esc key"
msgstr "Esc キーでファイル選択ダイアログを閉じれるように修正"

#: data/io.elementary.files.appdata.xml.in.in:54
msgid "Use new emblems for git status"
msgstr "Git ステータス表示に新しいエンブレムを使うように修正"

#: data/io.elementary.files.appdata.xml.in.in:55
msgid "Show selection actions in secondary click menu"
msgstr "副クリックメニューにファイル選択アクションを表示"

#: data/io.elementary.files.appdata.xml.in.in:59
msgid "Always show Permissions page in Properties dialog"
msgstr "“プロパティー”ダイアログに“権限”のページを常に表示するように修正"

#: data/io.elementary.files.appdata.xml.in.in:60
msgid ""
"In Permissions page, show user and group numeric IDs when names not available"
msgstr ""
"“権限”のページにおいて、ユーザー名とグループ名が利用できない場合は数値のユー"
"ザー ID とグループ ID を表示するように修正"

#: data/io.elementary.files.appdata.xml.in.in:61
msgid "In Permissions page, show message when no information available"
msgstr ""
"“権限”のページにおいて、情報が利用できない場合はメッセージを表示するように修"
"正"

#: data/io.elementary.files.appdata.xml.in.in:63
#: data/io.elementary.files.appdata.xml.in.in:108
#: data/io.elementary.files.appdata.xml.in.in:125
#: data/io.elementary.files.appdata.xml.in.in:140
#: data/io.elementary.files.appdata.xml.in.in:157
#: data/io.elementary.files.appdata.xml.in.in:185
#: data/io.elementary.files.appdata.xml.in.in:209
#: data/io.elementary.files.appdata.xml.in.in:228
#: data/io.elementary.files.appdata.xml.in.in:250
#: data/io.elementary.files.appdata.xml.in.in:303
msgid "Other updates:"
msgstr "そのほかのアップデート:"

#: data/io.elementary.files.appdata.xml.in.in:65
msgid "Only allow one FileChooser per parent window"
msgstr ""
"親ウィンドウ一つに対して複数のファイル選択ダイアログを開ける不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:66
msgid ""
"Navigate to folder when pressing enter in FileChooser instead of selecting"
msgstr ""
"ファイル選択ダイアログで Enter キーを押した際に、フォルダーを選択するのではな"
"くその中にある項目を表示するように修正"

#: data/io.elementary.files.appdata.xml.in.in:78
msgid ""
"Use \"Send Mail\" portal instead of contract, improving compatibility with "
"alternate email apps"
msgstr ""
"Contract ではなく“メールを送信”ポータルを使用するように修正し、ほかのメールア"
"プリとの互換性を改善"

#: data/io.elementary.files.appdata.xml.in.in:79
msgid "Add file filters and New Folder options to file chooser portal"
msgstr ""
"ファイル選択ポータルにファイルの種類を絞ったり新しいフォルダーを作成したりで"
"きるオプションを追加"

#: data/io.elementary.files.appdata.xml.in.in:80
msgid ""
"Allow blank passwords for remote connections, e.g. for SSH via a private key"
msgstr "リモート接続の空パスワードを許可 (プライベートキーでの SSH 接続など)"

#: data/io.elementary.files.appdata.xml.in.in:81
msgid "Make ejecting devices safer"
msgstr "デバイスをより安全に取り出せるように修正"

#: data/io.elementary.files.appdata.xml.in.in:82
msgid "Add option to stop drive if possible"
msgstr "可能であればドライブを停止できるオプションを追加"

#: data/io.elementary.files.appdata.xml.in.in:83
msgid "Show unformatted drives and drives without media"
msgstr "未フォーマットのドライブとメディアがないドライブを表示するように修正"

#: data/io.elementary.files.appdata.xml.in.in:87
msgid ""
"Fix pasting of selected pathbar text into another window using middle-click"
msgstr ""
"選択されたパスバーのテキストを中クリックで別ウィンドウに貼り付けできる機能を"
"追加"

#: data/io.elementary.files.appdata.xml.in.in:88
msgid "Fix selecting multiple groups of files"
msgstr "複数グループのファイルを選択する際の不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:89
msgid "Allow dropping bookmark directly below the Recent bookmark"
msgstr "“最近使用した項目”の直下にブックマークをドロップできるように修正"

#: data/io.elementary.files.appdata.xml.in.in:90
msgid "Do not show unusable drop target below the Trash bookmark"
msgstr "サイドバーの“ゴミ箱”の直下ではドロップマークを表示しないように修正"

#: data/io.elementary.files.appdata.xml.in.in:91
msgid "Fix sidebar showing both drive and volume for same device"
msgstr ""
"同じデバイスに対してドライブとボリュームの両方がサイドバーに表示されていた不"
"具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:92
msgid "Fix sidebar showing SSH servers in both Storage and Network sections"
msgstr ""
"SSH サーバーがサイドバーの“ストレージ”と“ネットワーク”のセクション両方に表示"
"されていた不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:104
msgid "Show translated bookmark names when changing languages"
msgstr "表示言語を変更した際にブックマーク名を変更後の言語で表示するように修正"

#: data/io.elementary.files.appdata.xml.in.in:105
msgid "Stop some audio file icons disappearing when scrolling or changing view"
msgstr ""
"スクロールしたりビューを切り替えたりすると、オーディオファイルのアイコンが消"
"えることがある不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:106
msgid ""
"Stop brief appearance of status overlay when changing root folder in Column "
"View"
msgstr ""
"カラム表示でルートフォルダーを変更すると状態オーバーレイが一瞬表示されていた"
"不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:110
msgid "Remove message about reporting issues when running from Terminal"
msgstr ""
"“ターミナル”からこのアプリを起動した際に表示される、不具合の報告先に関する"
"メッセージを削除"

#: data/io.elementary.files.appdata.xml.in.in:119
msgid "Open bookmarks in a new tab with Ctrl + Click"
msgstr "Ctrl + クリックでブックマークを新しいタブで開けるように修正"

#: data/io.elementary.files.appdata.xml.in.in:120
msgid "Fix dropping uris onto storage devices and network locations in sidebar"
msgstr ""
"サイドバー上のストレージデバイスやネットワークの場所に URI をドラッグアンドド"
"ロップできない不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:121
msgid "Fix restoring tabs after a system restart"
msgstr ""
"“ファイル”を閉じずにシステムを再起動した場合、タブが復元されない不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:122
msgid ""
"Don't unselect when secondary clicking blank space around a file or folder"
msgstr ""
"ファイルやフォルダー周辺の空白領域を副クリックした場合に、選択項目が解除され"
"る不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:123
msgid ""
"Show the folder context menu when secondary clicking outside of a selection"
msgstr ""
"選択項目以外の場所で副クリックした場合はフォルダーのコンテキストメニューを表"
"示するように修正"

#: data/io.elementary.files.appdata.xml.in.in:135
msgid "Fix small context menus on breadcrumbs"
msgstr ""
"ぱんくずリストのコンテキストメニューが小さく描画されてしまう不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:136
msgid "Fix bookmarking a single selected item with Ctrl + D"
msgstr "選択中の1項目を Ctrl + D キーでブックマークできない不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:137
msgid "Fix renaming bookmarks in the sidebar"
msgstr "サイドバーのブックマークを名称変更できない不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:138
msgid "Fix an issue with showing color tags when thumbnails are hidden"
msgstr ""
"サムネイルが非表示の場合に発生する、カラータグを表示する機能の不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:150
msgid "Fix freeze when comparing copied files"
msgstr "コピーしたファイルを比較する際にフリーズする不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:151
msgid "Fix truncation of final column in Column View under some circumstances"
msgstr ""
"特定の状況下において、カラム表示の最後の列が途切れて表示されてしまう不具合を"
"修正"

#: data/io.elementary.files.appdata.xml.in.in:155
msgid "Show more keyboard accelerators in menus"
msgstr "メニュー項目に表示するキーボードショートカットを追加"

#: data/io.elementary.files.appdata.xml.in.in:167
msgid "Fix connecting to AFP servers so that passwords are remembered"
msgstr "AFP サーバーへの接続時にパスワードを保存するように修正"

#: data/io.elementary.files.appdata.xml.in.in:168
msgid "Fix MTP mounts"
msgstr "MTP のマウントを修正"

#: data/io.elementary.files.appdata.xml.in.in:172
msgid "Launch files with double click instead of single click"
msgstr "ファイルをシングルクリックではなくダブルクリックで開くように修正"

#: data/io.elementary.files.appdata.xml.in.in:173
msgid "Provide a File Chooser portal for Flatpak apps"
msgstr "Flatpak アプリ向けにファイル選択ポータルを提供"

#: data/io.elementary.files.appdata.xml.in.in:174
msgid "Brand new animated Sidebar"
msgstr "アニメーション付きのサイドバーを提供"

#: data/io.elementary.files.appdata.xml.in.in:175
msgid "Dark style support"
msgstr "ダークスタイルへの対応"

#: data/io.elementary.files.appdata.xml.in.in:176
msgid "Mint and Bubblegum color tags"
msgstr "ミントとピンク色のタグを追加"

#: data/io.elementary.files.appdata.xml.in.in:177
msgid "Do not restore locations that have become inaccessible"
msgstr ""
"アクセス不可能になった場所をアプリ起動時やタブの復元時に表示しないように修正"

#: data/io.elementary.files.appdata.xml.in.in:178
msgid "Clicking between thumbnail and text now activates/selects in Grid view"
msgstr ""
"グリッド表示において、ファイルのサムネイルと名前の間をクリックした場合にファ"
"イルを選択するように変更"

#: data/io.elementary.files.appdata.xml.in.in:179
msgid "AFC protocol support"
msgstr "AFC プロトコルに対応"

#: data/io.elementary.files.appdata.xml.in.in:180
msgid "Add a smaller minimum icon size in list view"
msgstr "リスト表示でのアイコンにより小さいサイズを追加"

#: data/io.elementary.files.appdata.xml.in.in:181
msgid "Show emblems inline in list views"
msgstr "リスト表示でエンブレムをインライン表示するように変更"

#: data/io.elementary.files.appdata.xml.in.in:182
msgid "Performance improvements"
msgstr "パフォーマンスの改善"

#: data/io.elementary.files.appdata.xml.in.in:183
msgid "Stability improvements"
msgstr "安定性の改善"

#: data/io.elementary.files.appdata.xml.in.in:187
msgid "Rename \"Devices\" to \"Storage\""
msgstr "\"デバイス\"を\"ストレージ\"に名称変更"

#: data/io.elementary.files.appdata.xml.in.in:196
msgid "Bookmark menu option for network mounts in sidebar"
msgstr ""
"ネットワーク上のマウントされた場所に対しても、サイドバーのコンテキストメ"
"ニューからブックマークに登録できるように変更"

#: data/io.elementary.files.appdata.xml.in.in:197
msgid "Show folder item count in List View"
msgstr "リスト表示の場合に、フォルダー内の項目数を表示するように変更"

#: data/io.elementary.files.appdata.xml.in.in:198
msgid ""
"Now shows thumbnails on locally mounted MTP and PTP devices as well as on "
"network locations by default"
msgstr ""
"ネットワーク上の場所と同様に、ローカルにマウントされた MTP と PTP デバイスで"
"もデフォルトでサムネイルを表示するように変更"

#: data/io.elementary.files.appdata.xml.in.in:199
msgid ""
"Properties window: Allow the filename to be copied when it cannot be edited"
msgstr ""
"プロパティーウィンドウ: ファイル名が編集できない場合でも、コピーはできるよう"
"に修正"

#: data/io.elementary.files.appdata.xml.in.in:200
msgid "Improved tooltip formatting for devices in the sidebar"
msgstr "サイドバーのデバイスに対するツールチップの書式を改善"

#: data/io.elementary.files.appdata.xml.in.in:204
msgid "Fix crash when pressing Enter and another key at the same time"
msgstr "Enter キーとほかのキーを同時に押した場合にクラッシュする不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:205
msgid "Fix pathbar handling of \"~\" and \"..\" in path"
msgstr "パスに \"~\" や \"..\" が含まれる場合のパスバーの処理を修正"

#: data/io.elementary.files.appdata.xml.in.in:206
msgid "Filechooser pathbar no longer crashes when invoked from a Flatpak"
msgstr ""
"ファイル選択ダイアログを Flatpak アプリから呼び出し、パスバーにポインターを移"
"動すると、クラッシュする不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:207
msgid ""
"Do not show git status for repositories on FUSE filesystems to prevent "
"possible blocking"
msgstr ""
"アクセスがブロックされる可能性があるため、FUSE ファイルシステム上に保存された"
"レポジトリに対して、Git ステータスを表示しないように変更"

#: data/io.elementary.files.appdata.xml.in.in:211
msgid "\"Personal\" is now \"Bookmarks\""
msgstr "\"パーソナル\"を\"ブックマーク\"に名称変更"

#: data/io.elementary.files.appdata.xml.in.in:220
msgid ""
"Add warning and error colored disk usage bars when disk becomes too full"
msgstr ""
"ディスク容量が限界に近くなった場合に、ディスク使用量バーを黄色や赤色で表示す"
"るように変更"

#: data/io.elementary.files.appdata.xml.in.in:221
msgid ""
"Prevent window resizing when filename column width exceeds available space"
msgstr ""
"ファイル名の列の幅が利用可能な領域よりも長くなった場合に、ウィンドウがリサイ"
"ズしないように修正"

#: data/io.elementary.files.appdata.xml.in.in:225
msgid "Fix handling of filenames containing the # character"
msgstr "ファイル名に # 記号が含まれている場合の処理を修正"

#: data/io.elementary.files.appdata.xml.in.in:226
msgid "Fix regressions regarding pathbar context menus and clicking"
msgstr "パスバーのコンテキストメニューとクリックに関するリグレッションを修正"

#: data/io.elementary.files.appdata.xml.in.in:237
msgid "Paste images into other apps instead of file paths where possible"
msgstr ""
"ほかのアプリに貼り付ける際に、可能であればファイルパスではなく画像自体を貼り"
"付けるように変更"

#: data/io.elementary.files.appdata.xml.in.in:238
msgid "Paste into a selected folder when using Ctrl + V"
msgstr "Ctrl + V を使用して、選択したフォルダーの中に貼り付けできるように変更"

#: data/io.elementary.files.appdata.xml.in.in:239
msgid "Show file info overlay in List View as well"
msgstr "リスト表示の場合でも、ファイル情報オーバーレイを表示するように変更"

#: data/io.elementary.files.appdata.xml.in.in:240
msgid "Traverse search results with Tab key"
msgstr "検索結果内を Tab キーで移動できるように変更"

#: data/io.elementary.files.appdata.xml.in.in:241
msgid "Show an error message when attempting to open trashed files"
msgstr "ゴミ箱のファイルを開こうとした際にエラーメッセージを表示するように変更"

#: data/io.elementary.files.appdata.xml.in.in:245
msgid "Fix uneditable area in pathbar which is showing home folder placeholder"
msgstr ""
"パスバーにある、ホームフォルダーのプレースホルダー表示部でクリックしてテキス"
"トを入力できない不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:246
msgid "Fix an issue that prevented file modification times from showing"
msgstr "ファイル修正日時が表示されない不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:247
msgid "Fix size of restored tiled window"
msgstr ""
"タイル表示されたウィンドウのサイズが次回起動時に正しく復元されない不具合を修"
"正"

#: data/io.elementary.files.appdata.xml.in.in:248
msgid "Fix color tags disappearing when thumbnails hidden"
msgstr "サムネイルが非表示の場合に、カラータグが消滅する不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:260
msgid "Fix crash when a device icon is coming from a file"
msgstr "デバイスアイコンがファイルである場合のクラッシュを修正"

#: data/io.elementary.files.appdata.xml.in.in:261
msgid "Fix device icon sometimes missing"
msgstr "デバイスアイコンが表示されないことがある不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:262
msgid "Fix occasional view freeze after renaming"
msgstr "名前を変更するとビューがフリーズすることがある不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:263
msgid "Improve renaming logic when dealing with leading/trailing whitespace"
msgstr "ファイル名の先頭または末尾に空白を含む場合の、名前の変更処理を改善"

#: data/io.elementary.files.appdata.xml.in.in:264
msgid "Fix breadcrumbs sometimes incorrect at startup"
msgstr "起動時にパンくずリストの表示が間違っていることがある不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:265
msgid "Do not show file:// prefix in navigation buttons menus"
msgstr ""
"ナビゲーションボタンのメニューに file:// という接尾辞を表示しないように修正"

#: data/io.elementary.files.appdata.xml.in.in:274
msgid "Fix \"New Folder\" keyboard shortcut label in menu"
msgstr ""
"メニューにある\"新しいフォルダー\"項目のキーボードショートカットの表示が間"
"違っていた不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:275
msgid "Fix navigation with back/forward button context menu"
msgstr ""
"戻る／進むボタンのコンテキストメニューでのナビゲーションに関する不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:276
msgid "Fix path bar sometimes showing wrong path when closing a tab"
msgstr "タブを閉じるとパスバーに間違ったパスが表示されることがある不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:277
msgid ""
"Ensure keyboard shortcuts work immediately after creating or renaming a file"
msgstr ""
"ファイルの作成・名前の変更直後でもキーボードショートカットが機能するように修"
"正"

#: data/io.elementary.files.appdata.xml.in.in:278
msgid "Do not include \"file://\" in pathbar text or when pasting path as text"
msgstr ""
"パスバーのテキストやパスをテキストとして貼り付けする際に、文字列 \"file://\" "
"を含まないように修正"

#: data/io.elementary.files.appdata.xml.in.in:286
msgid "Show keyboard shortcuts in menu items"
msgstr "メニュー項目にキーボードショートカットを表示するように修正"

#: data/io.elementary.files.appdata.xml.in.in:287
msgid "Fix an issue with breadcrumbs in the file chooser"
msgstr "ファイル選択ダイアログのパンくずリストに関する不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:288
msgid "Show a warning when ejecting a volume that's still in use"
msgstr "使用中のボリュームを取り出そうとした際に警告を表示するように修正"

#: data/io.elementary.files.appdata.xml.in.in:289
msgid "Fix cursor update after horizontal scroll in Column View"
msgstr "カラム表示で水平スクロールを行った後、カーソルを更新するように修正"

#: data/io.elementary.files.appdata.xml.in.in:290
msgid "Fix folder-open icon persisting after closing Column View"
msgstr ""
"カラム表示を閉じた後に folder-open アイコンが表示され続ける不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:291
msgid "Use destructive action styling for some trash dialog buttons"
msgstr "ファイルの削除に関するダイアログのボタンを赤色で強調表示するように修正"

#: data/io.elementary.files.appdata.xml.in.in:298
msgid "New features:"
msgstr "新機能:"

#: data/io.elementary.files.appdata.xml.in.in:300
msgid "Initial git plugin support"
msgstr "Git プラグインへの対応を開始"

#: data/io.elementary.files.appdata.xml.in.in:301
msgid "Follow global history setting when restoring and saving tabs"
msgstr "タブを復元・保存する際にシステムの履歴設定に準拠するように変更"

#: data/io.elementary.files.appdata.xml.in.in:305
msgid "Ensure tabs showing same folder remain synchronized"
msgstr "同じフォルダーを表示しているタブが正しく同期されるように修正"

#: data/io.elementary.files.appdata.xml.in.in:306
msgid "Fix drag with secondary button on empty space"
msgstr "副ボタンで何もないエリアにドラッグした際の不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:307
msgid "Show custom media type icons for installed apps"
msgstr ""
"インストール済みのアプリが、独自のメディアタイプアイコンを表示できるように修"
"正"

#: data/io.elementary.files.appdata.xml.in.in:308
msgid "Fix appearance of inactive diskspace indicator"
msgstr "非アクティブ状態のディスク容量バーの見た目を修正"

#: data/io.elementary.files.appdata.xml.in.in:309
msgid "Improve pathbar animation"
msgstr "パスバーのアニメーションを改善"

#: data/io.elementary.files.appdata.xml.in.in:310
msgid "Update documentation"
msgstr "ドキュメンテーションの更新"

#: data/io.elementary.files.appdata.xml.in.in:311
msgid "Update translations"
msgstr "翻訳を更新"

#: data/io.elementary.files.appdata.xml.in.in:318
msgid "Initial cloudproviders plugin support"
msgstr "クラウドプロバイダープラグインへの対応を開始"

#: data/io.elementary.files.appdata.xml.in.in:319
msgid "Fix selecting pasted files"
msgstr "貼り付けたファイルを選択する際の不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:320
msgid "Fix color label visibility while using dark theme"
msgstr "ダークテーマ使用時に、カラーラベルが見にくい不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:321
msgid "Fix selecting files using Shift key"
msgstr "Shift キーを使ってファイルを選択する際の不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:322
msgid "Draw checkerboard background for image items"
msgstr "画像項目にチェック柄の背景を描画するように変更"

#: data/io.elementary.files.appdata.xml.in.in:323
msgid "Improved styling for disk space bars"
msgstr "ディスク容量バーのスタイルを改善"

#: data/io.elementary.files.appdata.xml.in.in:331
msgid "Show more search results"
msgstr "検索結果の表示量を増加"

#: data/io.elementary.files.appdata.xml.in.in:332
msgid "Ensure valid tab name generation"
msgstr "タブ名が正しく表示されるように修正"

#: data/io.elementary.files.appdata.xml.in.in:333
msgid "Properly sort folders by date and size"
msgstr "フォルダーを日時とサイズで正しく並べ替えるように修正"

#: data/io.elementary.files.appdata.xml.in.in:334
msgid "Launching Files from Terminal now opens new tab instead of new window"
msgstr ""
"“ファイル”から“ターミナル”を起動すると、新しいウィンドウではなく新しいタブで"
"開かれるように変更"

#: data/io.elementary.files.appdata.xml.in.in:335
msgid "Improve MTP support"
msgstr "MTP への対応を改善"

#: data/io.elementary.files.appdata.xml.in.in:336
msgid "Various tagging feature refinements"
msgstr "タグ機能のさまざまな改善"

#: data/io.elementary.files.appdata.xml.in.in:337
msgid "Keyboard navigation improvements"
msgstr "キーボード操作を改善"

#: data/io.elementary.files.appdata.xml.in.in:338
msgid "Symlink copy and paste fixes"
msgstr "シンボリックリンクのコピー・貼り付けを修正"

#: data/io.elementary.files.appdata.xml.in.in:339
msgid "Avoid crash when Templates folder is null"
msgstr "“テンプレート”フォルダーが空の場合にクラッシュする不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:340
msgid "Reduce memory footprint of FileChooserDialog"
msgstr "ファイル選択ダイアログのメモリー使用量を削減"

#: data/io.elementary.files.appdata.xml.in.in:341
msgid "Avoid crash when selecting image file in some situations"
msgstr ""
"特定の状況下において、画像ファイルを選択した際にアプリがクラッシュする不具合"
"を修正"

#: data/io.elementary.files.appdata.xml.in.in:342
msgid ""
"Fix unclosable progress window while copying network files to removable "
"storage"
msgstr ""
"ネットワークのファイルをリムーバブルストレージにコピーする際に、進捗ウィンド"
"ウを閉じられない不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:343
msgid "Fix drag and drop after double clicking a blank area"
msgstr ""
"何もない領域をダブルクリックすると、ドラッグアンドドロップができない不具合を"
"修正"

#: data/io.elementary.files.appdata.xml.in.in:344
msgid "Avoid crash while scrolling over view switcher"
msgstr "表示方法切り替えボタン上でスクロールするとクラッシュする不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:345
msgid "Avoid possible crash in some copy and paste situations"
msgstr "コピー・貼り付けの際に場合によってはクラッシュする不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:353
msgid "Keyboard navigation fix for cherry picking select files in Grid view"
msgstr "グリッド表示で、ファイルを一項目ずつ選択する際のキーボード操作を修正"

#: data/io.elementary.files.appdata.xml.in.in:354
msgid "Don't hardcode search placeholder text style, fixes dark theme issue"
msgstr ""
"ダークテーマにおいて検索プレースホルダーのテキストが見にくい不具合を、テキス"
"トスタイルのハードコードを削除することで修正"

#: data/io.elementary.files.appdata.xml.in.in:362
msgid "Minor spacing adjustments to location bar"
msgstr "場所バーの領域を微調整"

#: data/io.elementary.files.appdata.xml.in.in:363
msgid "Fix F2 renaming behavior"
msgstr "F2 キーによる名前変更の挙動を修正"

#: data/io.elementary.files.appdata.xml.in.in:371
msgid "Minor spacing adjustments to location breadcrumbs"
msgstr "場所パンくずリストの領域を微調整"

#: data/io.elementary.files.appdata.xml.in.in:372
msgid "Consistently remember color tags"
msgstr "カラータグが正しく保持されるように修正"

#: data/io.elementary.files.appdata.xml.in.in:373
msgid ""
"Reload thumbnails when changing zoom level to avoid showing placeholder "
"unnecessarily"
msgstr ""
"プレースホルダーが意図せず表示される不具合を修正するため、拡大レベルを変更し"
"た際にサムネイルを再読み込みするように変更"

#: data/io.elementary.files.appdata.xml.in.in:374
msgid "Fix running scripts with spaces in filename"
msgstr "ファイル名に空白が含まれるスクリプトを実行できない不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:382
msgid "Fix high CPU regression in version 4.1.4"
msgstr "バージョン 4.1.4 で確認された、CPU 使用率が高くなる不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:383
msgid "File sorting fixes"
msgstr "ファイルの並べ替えの不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:391
msgid "Meson: fix soversion names"
msgstr "Meson: soversion 名を修正"

#: data/io.elementary.files.appdata.xml.in.in:392
msgid "Remove CMake build system"
msgstr "CMake ビルドシステムを削除"

#: data/io.elementary.files.appdata.xml.in.in:393
msgid "Fix missing File System properties menu"
msgstr "ルートフォルダーにおいてプロパティーメニューが表示されない不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:394
msgid "Drop intltool dependency"
msgstr "intltool を依存関係から削除"

#: data/io.elementary.files.appdata.xml.in.in:395
msgid "Fix default file type handler logic"
msgstr "デフォルトのファイルタイプハンドラーの仕組みを修正"

#: data/io.elementary.files.appdata.xml.in.in:396
msgid "Fix initial search window size too small"
msgstr ""
"検索ウィンドウを初めて使用する際に、ウィンドウサイズが小さすぎる不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:397
msgid "Add option to disable local file thumbnails"
msgstr "ローカルファイルのサムネイルを無効化するオプションを追加"

#: data/io.elementary.files.appdata.xml.in.in:398
msgid "Fix crash when opening multiple video files"
msgstr "複数の動画ファイルを開く際のクラッシュを修正"

#: data/io.elementary.files.appdata.xml.in.in:399
msgid "Fix some timing issues around adding and removing files"
msgstr "ファイルの追加・削除のタイミングに関する不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:400
msgid "Meson: fix missing library headers"
msgstr "Meson: 欠如していたライブラリのヘッダーを追加"

#: data/io.elementary.files.appdata.xml.in.in:401
msgid "Fix color tag display"
msgstr "カラータグの表示を修正"

#: data/io.elementary.files.appdata.xml.in.in:402
msgid "Show reserved space in Properties window"
msgstr "“プロパティー”ウィンドウで使用済み領域を表示するように修正"

#: data/io.elementary.files.appdata.xml.in.in:410
msgid "Add border radius to text background"
msgstr "テキスト背景に角丸を追加"

#: data/io.elementary.files.appdata.xml.in.in:411
msgid "Fix file mimetype association regression"
msgstr "ファイルのメディアタイプの関連付けに関するリグレッションを修正"

#: data/io.elementary.files.appdata.xml.in.in:412
msgid "New pathbar tooltips"
msgstr "パスバーの新しいツールチップ"

#: data/io.elementary.files.appdata.xml.in.in:413
msgid "Build fixes"
msgstr "ビルドの不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:421
msgid "Plugin build fixes"
msgstr "プラグインのビルドの不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:429
msgid "Fix Ctrl+Tab behavior"
msgstr "Ctrl+Tab の挙動を修正"

#: data/io.elementary.files.appdata.xml.in.in:430
msgid "Fix building without Unity library"
msgstr "Unity ライブラリを使用しないとビルドできない不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:437
msgid "Hide breadcrumbs and show placeholder and search icon in home folder"
msgstr ""
"ホームフォルダーでは、パンくずリストを非表示にし、プレースホルダーと検索アイ"
"コンを表示するように修正"

#: data/io.elementary.files.appdata.xml.in.in:438
msgid "Style error dialogs"
msgstr "エラーダイアログのスタイルを変更"

#: data/io.elementary.files.appdata.xml.in.in:439
msgid "Right click fixes"
msgstr "副ボタンクリック時の挙動を修正"

#: data/io.elementary.files.appdata.xml.in.in:440
msgid "Reload recent view when privacy setting changes"
msgstr ""
"プライバシーの設定の変更時に、最近使用した項目を再読み込みするように修正"

#: data/io.elementary.files.appdata.xml.in.in:441
msgid "`Connect Server` fixes"
msgstr "`サーバーに接続` を修正"

#: data/io.elementary.files.appdata.xml.in.in:442
msgid "Do not activate multiple files with single click"
msgstr "シングルクリックで複数のファイルを選択しないように修正"

#: data/io.elementary.files.appdata.xml.in.in:443
msgid "Use `Tab` to toggle View and Sidebar keyboard focus"
msgstr "ビューとサイドバー間のフォーカス変更に `Tab` キーを使用するように変更"

#: data/io.elementary.files.appdata.xml.in.in:444
msgid "Delete color tag database entries for trashed files"
msgstr "項目がゴミ箱に入ったら、データベースからカラータグを削除するように修正"

#: data/io.elementary.files.appdata.xml.in.in:445
msgid "Throttle tab closing"
msgstr ""
"Ctrl + W でタブを閉じてから一定時間経過していない場合は、タブを閉じないように"
"変更"

#: data/io.elementary.files.appdata.xml.in.in:446
msgid "`Ctrl` key fixes"
msgstr "`Ctrl` キー押下時の挙動を修正"

#: data/io.elementary.files.appdata.xml.in.in:447
msgid "Drag and drop fixes"
msgstr "ドラッグアンドドロップ時の挙動を修正"

#: data/io.elementary.files.appdata.xml.in.in:448
msgid "Show icons in `Open with` menus"
msgstr ""
"`このアプリケーションで開く` メニューの項目にアイコンを表示するように変更"

#: data/io.elementary.files.appdata.xml.in.in:449
msgid "Tooltip fixes"
msgstr "ツールチップの修正"

#: data/io.elementary.files.appdata.xml.in.in:450
msgid "Memory leak fixes"
msgstr "メモリーリークを修正"

#: data/io.elementary.files.appdata.xml.in.in:451
msgid "Crash fixes"
msgstr "クラッシュを修正"

#: data/io.elementary.files.appdata.xml.in.in:452
msgid "Trash: respect sound setting"
msgstr ""
"システムで“イベント警告”の設定がオフの場合は、ゴミ箱を空にする音を鳴らさない"
"ように修正"

#: data/io.elementary.files.appdata.xml.in.in:453
msgid "Localization fixes"
msgstr "地域化に関する修正"

#: data/io.elementary.files.appdata.xml.in.in:461
msgid "Over 100 major and minor bug fixes and improvements"
msgstr "100件以上の大規模または軽微な不具合修正と改善"

#: data/io.elementary.files.appdata.xml.in.in:468
msgid "Honor 12/24hr system setting"
msgstr "システムの 12時制／24時制の設定に従うように修正"

#: data/io.elementary.files.appdata.xml.in.in:469
msgid "Distinguish between tabs with the same name"
msgstr "同じ名前のフォルダを別のタブで表示している場合に区別できるように修正"

#: data/io.elementary.files.appdata.xml.in.in:470
msgid "Support launching from other applications with a target file selected"
msgstr "選択されたファイルを使用してのほかのアプリの起動に対応"

#: data/io.elementary.files.appdata.xml.in.in:471
#: data/io.elementary.files.appdata.xml.in.in:484
#: data/io.elementary.files.appdata.xml.in.in:498
#: data/io.elementary.files.appdata.xml.in.in:508
#: data/io.elementary.files.appdata.xml.in.in:519
msgid "New translations"
msgstr "新しい翻訳"

#: data/io.elementary.files.appdata.xml.in.in:472
#: data/io.elementary.files.appdata.xml.in.in:485
#: data/io.elementary.files.appdata.xml.in.in:499
#: data/io.elementary.files.appdata.xml.in.in:509
#: data/io.elementary.files.appdata.xml.in.in:520
#: data/io.elementary.files.appdata.xml.in.in:527
#: data/io.elementary.files.appdata.xml.in.in:534
#: data/io.elementary.files.appdata.xml.in.in:558
msgid "Minor bug fixes"
msgstr "軽微なバグの修正"

#: data/io.elementary.files.appdata.xml.in.in:479
msgid "Improved networking support"
msgstr "ネットワークへの対応を改善"

#: data/io.elementary.files.appdata.xml.in.in:480
msgid "Correct window geometry when snapping to left or right"
msgstr ""
"ウィンドウを左右にスナップしたときに正しいジオメトリー処理が行われるように修"
"正"

#: data/io.elementary.files.appdata.xml.in.in:481
msgid ""
"Pressing Ctrl no longer cancels renaming while the \"Reveal Pointer\" "
"setting is active"
msgstr ""
"\"ポインターのハイライト\"の設定が有効な間に Ctrl キーを押すことで名前の変更"
"がキャンセルされないように修正"

#: data/io.elementary.files.appdata.xml.in.in:482
msgid "Switching input language cancels actions"
msgstr "入力言語の切り替えが操作を中止"

#: data/io.elementary.files.appdata.xml.in.in:483
#: data/io.elementary.files.appdata.xml.in.in:497
#: data/io.elementary.files.appdata.xml.in.in:507
#: data/io.elementary.files.appdata.xml.in.in:518
msgid "Various crash fixes"
msgstr "さまざまなクラッシュの修正"

#: data/io.elementary.files.appdata.xml.in.in:492
msgid ""
"Web browsers like Firefox now remember the most recently used downloads "
"directory"
msgstr ""
"Firefox などの Web ブラウザーが最近使用されたダウンロードディレクトリを記憶す"
"るようになりました"

#: data/io.elementary.files.appdata.xml.in.in:493
msgid "Remember preferred zoom level"
msgstr "選択されたズームレベルを記憶"

#: data/io.elementary.files.appdata.xml.in.in:494
msgid "Improved input method support"
msgstr "インプットメソッドへの対応を改善"

#: data/io.elementary.files.appdata.xml.in.in:495
msgid "910x640 minimum window size"
msgstr "最小 910x640 のウィンドウサイズ"

#: data/io.elementary.files.appdata.xml.in.in:496
msgid "Security fixes"
msgstr "セキュリティの修正"

#: data/io.elementary.files.appdata.xml.in.in:506
msgid "Enable drag and drop tabs between windows"
msgstr "ウィンドウ間のタブのドラッグアンドドロップを有効化"

#: data/io.elementary.files.appdata.xml.in.in:516
msgid "Merge search functionality into Ctrl+F"
msgstr "検索機能を Ctrl + F に統合"

#: data/io.elementary.files.appdata.xml.in.in:517
msgid "Improved translation support"
msgstr "翻訳への対応を改善"

#: data/io.elementary.files.appdata.xml.in.in:541
msgid "Fix appdata release dates"
msgstr "AppData のリリース日を修正"

#: data/io.elementary.files.appdata.xml.in.in:548
msgid "Improve file opening over Samba shares"
msgstr "Samba 共有上でのファイルのオープンを改善"

#: data/io.elementary.files.appdata.xml.in.in:549
msgid "Fix a crash when restoring items from the Trash"
msgstr "ゴミ箱からアイテムを復元する際にクラッシュする不具合を修正"

#: data/io.elementary.files.appdata.xml.in.in:550
msgid "Improve cut/copy/paste sensitivity in context menu"
msgstr "コンテキストメニューにおける切り取り・コピー・貼り付けの感度を改善"

#: data/io.elementary.files.appdata.xml.in.in:551
msgid "Translation updates"
msgstr "翻訳の更新"

#: data/io.elementary.files.appdata.xml.in.in:563
msgid "elementary, Inc."
msgstr "elementary, Inc."

#: data/io.elementary.files.policy.in.in:10
msgid "Run Pantheon Files as Administrator"
msgstr "“ファイル”を管理者として実行"

#: data/io.elementary.files.policy.in.in:11
msgid "Authentication is required to run Files as Administrator"
msgstr "“ファイル”を管理者として実行するには認証が必要です"

#~ msgid "About Files"
#~ msgstr "\"ファイル\" について"
