msgid ""
msgstr ""
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-24 20:43+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"

#: data/io.elementary.files.desktop.in.in:3
#: data/io.elementary.files.appdata.xml.in.in:8
msgid "Files"
msgstr ""

#: data/io.elementary.files.desktop.in.in:4
msgid "Browse your files"
msgstr ""

#: data/io.elementary.files.desktop.in.in:5
msgid "folder;manager;explore;disk;filesystem;"
msgstr ""

#: data/io.elementary.files.desktop.in.in:6
msgid "File Manager"
msgstr ""

#. TRANSLATORS This string is an icon name and should not be translated.
#: data/io.elementary.files.desktop.in.in:9
msgid "system-file-manager"
msgstr ""

#: data/io.elementary.files.desktop.in.in:20
msgid "New Window"
msgstr ""

#: data/io.elementary.files.desktop.in.in:24
msgid "New Window As Administrator"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:9
msgid "Browse and manage files and folders"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:11
msgid ""
"Easily copy, move, and rename your files, or use folders to stay organized. "
"Whether you like files in lists, grids or columns, you can always find them "
"right away. Access all your files, whether locally, on an external device or "
"remotely using FTP, SFTP, AFP, Webdav, or Windows share."
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:33
#: data/io.elementary.files.appdata.xml.in.in:51
#: data/io.elementary.files.appdata.xml.in.in:76
#: data/io.elementary.files.appdata.xml.in.in:153
#: data/io.elementary.files.appdata.xml.in.in:170
#: data/io.elementary.files.appdata.xml.in.in:194
#: data/io.elementary.files.appdata.xml.in.in:218
msgid "Improvements:"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:35
msgid "Show New Tab and New Window shortcuts in context menus"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:37
#: data/io.elementary.files.appdata.xml.in.in:57
#: data/io.elementary.files.appdata.xml.in.in:85
#: data/io.elementary.files.appdata.xml.in.in:102
#: data/io.elementary.files.appdata.xml.in.in:117
#: data/io.elementary.files.appdata.xml.in.in:133
#: data/io.elementary.files.appdata.xml.in.in:148
#: data/io.elementary.files.appdata.xml.in.in:165
#: data/io.elementary.files.appdata.xml.in.in:202
#: data/io.elementary.files.appdata.xml.in.in:223
#: data/io.elementary.files.appdata.xml.in.in:243
msgid "Fixes:"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:39
msgid "Double click selects instead of exiting while renaming in list view"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:40
msgid "Show public share icon in breadcrumbs"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:41
msgid "Prevent a crash when dragging to re-arrange bookmarks"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:43
#: data/io.elementary.files.appdata.xml.in.in:68
#: data/io.elementary.files.appdata.xml.in.in:94
#: data/io.elementary.files.appdata.xml.in.in:258
#: data/io.elementary.files.appdata.xml.in.in:272
msgid "Minor updates:"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:45
#: data/io.elementary.files.appdata.xml.in.in:70
#: data/io.elementary.files.appdata.xml.in.in:96
#: data/io.elementary.files.appdata.xml.in.in:111
#: data/io.elementary.files.appdata.xml.in.in:127
#: data/io.elementary.files.appdata.xml.in.in:142
#: data/io.elementary.files.appdata.xml.in.in:159
#: data/io.elementary.files.appdata.xml.in.in:188
#: data/io.elementary.files.appdata.xml.in.in:212
#: data/io.elementary.files.appdata.xml.in.in:230
#: data/io.elementary.files.appdata.xml.in.in:252
#: data/io.elementary.files.appdata.xml.in.in:266
#: data/io.elementary.files.appdata.xml.in.in:279
#: data/io.elementary.files.appdata.xml.in.in:292
#: data/io.elementary.files.appdata.xml.in.in:324
#: data/io.elementary.files.appdata.xml.in.in:346
#: data/io.elementary.files.appdata.xml.in.in:355
#: data/io.elementary.files.appdata.xml.in.in:364
#: data/io.elementary.files.appdata.xml.in.in:375
#: data/io.elementary.files.appdata.xml.in.in:384
#: data/io.elementary.files.appdata.xml.in.in:403
#: data/io.elementary.files.appdata.xml.in.in:414
#: data/io.elementary.files.appdata.xml.in.in:422
#: data/io.elementary.files.appdata.xml.in.in:454
msgid "Updated translations"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:53
msgid "Close FileChooser with Esc key"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:54
msgid "Use new emblems for git status"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:55
msgid "Show selection actions in secondary click menu"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:59
msgid "Always show Permissions page in Properties dialog"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:60
msgid ""
"In Permissions page, show user and group numeric IDs when names not available"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:61
msgid "In Permissions page, show message when no information available"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:63
#: data/io.elementary.files.appdata.xml.in.in:108
#: data/io.elementary.files.appdata.xml.in.in:125
#: data/io.elementary.files.appdata.xml.in.in:140
#: data/io.elementary.files.appdata.xml.in.in:157
#: data/io.elementary.files.appdata.xml.in.in:185
#: data/io.elementary.files.appdata.xml.in.in:209
#: data/io.elementary.files.appdata.xml.in.in:228
#: data/io.elementary.files.appdata.xml.in.in:250
#: data/io.elementary.files.appdata.xml.in.in:303
msgid "Other updates:"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:65
msgid "Only allow one FileChooser per parent window"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:66
msgid ""
"Navigate to folder when pressing enter in FileChooser instead of selecting"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:78
msgid ""
"Use \"Send Mail\" portal instead of contract, improving compatibility with "
"alternate email apps"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:79
msgid "Add file filters and New Folder options to file chooser portal"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:80
msgid ""
"Allow blank passwords for remote connections, e.g. for SSH via a private key"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:81
msgid "Make ejecting devices safer"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:82
msgid "Add option to stop drive if possible"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:83
msgid "Show unformatted drives and drives without media"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:87
msgid ""
"Fix pasting of selected pathbar text into another window using middle-click"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:88
msgid "Fix selecting multiple groups of files"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:89
msgid "Allow dropping bookmark directly below the Recent bookmark"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:90
msgid "Do not show unusable drop target below the Trash bookmark"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:91
msgid "Fix sidebar showing both drive and volume for same device"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:92
msgid "Fix sidebar showing SSH servers in both Storage and Network sections"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:104
msgid "Show translated bookmark names when changing languages"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:105
msgid "Stop some audio file icons disappearing when scrolling or changing view"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:106
msgid ""
"Stop brief appearance of status overlay when changing root folder in Column "
"View"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:110
msgid "Remove message about reporting issues when running from Terminal"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:119
msgid "Open bookmarks in a new tab with Ctrl + Click"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:120
msgid "Fix dropping uris onto storage devices and network locations in sidebar"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:121
msgid "Fix restoring tabs after a system restart"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:122
msgid ""
"Don't unselect when secondary clicking blank space around a file or folder"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:123
msgid ""
"Show the folder context menu when secondary clicking outside of a selection"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:135
msgid "Fix small context menus on breadcrumbs"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:136
msgid "Fix bookmarking a single selected item with Ctrl + D"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:137
msgid "Fix renaming bookmarks in the sidebar"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:138
msgid "Fix an issue with showing color tags when thumbnails are hidden"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:150
msgid "Fix freeze when comparing copied files"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:151
msgid "Fix truncation of final column in Column View under some circumstances"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:155
msgid "Show more keyboard accelerators in menus"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:167
msgid "Fix connecting to AFP servers so that passwords are remembered"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:168
msgid "Fix MTP mounts"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:172
msgid "Launch files with double click instead of single click"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:173
msgid "Provide a File Chooser portal for Flatpak apps"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:174
msgid "Brand new animated Sidebar"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:175
msgid "Dark style support"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:176
msgid "Mint and Bubblegum color tags"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:177
msgid "Do not restore locations that have become inaccessible"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:178
msgid "Clicking between thumbnail and text now activates/selects in Grid view"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:179
msgid "AFC protocol support"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:180
msgid "Add a smaller minimum icon size in list view"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:181
msgid "Show emblems inline in list views"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:182
msgid "Performance improvements"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:183
msgid "Stability improvements"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:187
msgid "Rename \"Devices\" to \"Storage\""
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:196
msgid "Bookmark menu option for network mounts in sidebar"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:197
msgid "Show folder item count in List View"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:198
msgid ""
"Now shows thumbnails on locally mounted MTP and PTP devices as well as on "
"network locations by default"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:199
msgid ""
"Properties window: Allow the filename to be copied when it cannot be edited"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:200
msgid "Improved tooltip formatting for devices in the sidebar"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:204
msgid "Fix crash when pressing Enter and another key at the same time"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:205
msgid "Fix pathbar handling of \"~\" and \"..\" in path"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:206
msgid "Filechooser pathbar no longer crashes when invoked from a Flatpak"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:207
msgid ""
"Do not show git status for repositories on FUSE filesystems to prevent "
"possible blocking"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:211
msgid "\"Personal\" is now \"Bookmarks\""
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:220
msgid ""
"Add warning and error colored disk usage bars when disk becomes too full"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:221
msgid ""
"Prevent window resizing when filename column width exceeds available space"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:225
msgid "Fix handling of filenames containing the # character"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:226
msgid "Fix regressions regarding pathbar context menus and clicking"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:237
msgid "Paste images into other apps instead of file paths where possible"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:238
msgid "Paste into a selected folder when using Ctrl + V"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:239
msgid "Show file info overlay in List View as well"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:240
msgid "Traverse search results with Tab key"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:241
msgid "Show an error message when attempting to open trashed files"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:245
msgid "Fix uneditable area in pathbar which is showing home folder placeholder"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:246
msgid "Fix an issue that prevented file modification times from showing"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:247
msgid "Fix size of restored tiled window"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:248
msgid "Fix color tags disappearing when thumbnails hidden"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:260
msgid "Fix crash when a device icon is coming from a file"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:261
msgid "Fix device icon sometimes missing"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:262
msgid "Fix occasional view freeze after renaming"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:263
msgid "Improve renaming logic when dealing with leading/trailing whitespace"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:264
msgid "Fix breadcrumbs sometimes incorrect at startup"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:265
msgid "Do not show file:// prefix in navigation buttons menus"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:274
msgid "Fix \"New Folder\" keyboard shortcut label in menu"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:275
msgid "Fix navigation with back/forward button context menu"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:276
msgid "Fix path bar sometimes showing wrong path when closing a tab"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:277
msgid ""
"Ensure keyboard shortcuts work immediately after creating or renaming a file"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:278
msgid "Do not include \"file://\" in pathbar text or when pasting path as text"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:286
msgid "Show keyboard shortcuts in menu items"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:287
msgid "Fix an issue with breadcrumbs in the file chooser"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:288
msgid "Show a warning when ejecting a volume that's still in use"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:289
msgid "Fix cursor update after horizontal scroll in Column View"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:290
msgid "Fix folder-open icon persisting after closing Column View"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:291
msgid "Use destructive action styling for some trash dialog buttons"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:298
msgid "New features:"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:300
msgid "Initial git plugin support"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:301
msgid "Follow global history setting when restoring and saving tabs"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:305
msgid "Ensure tabs showing same folder remain synchronized"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:306
msgid "Fix drag with secondary button on empty space"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:307
msgid "Show custom media type icons for installed apps"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:308
msgid "Fix appearance of inactive diskspace indicator"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:309
msgid "Improve pathbar animation"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:310
msgid "Update documentation"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:311
msgid "Update translations"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:318
msgid "Initial cloudproviders plugin support"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:319
msgid "Fix selecting pasted files"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:320
msgid "Fix color label visibility while using dark theme"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:321
msgid "Fix selecting files using Shift key"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:322
msgid "Draw checkerboard background for image items"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:323
msgid "Improved styling for disk space bars"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:331
msgid "Show more search results"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:332
msgid "Ensure valid tab name generation"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:333
msgid "Properly sort folders by date and size"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:334
msgid "Launching Files from Terminal now opens new tab instead of new window"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:335
msgid "Improve MTP support"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:336
msgid "Various tagging feature refinements"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:337
msgid "Keyboard navigation improvements"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:338
msgid "Symlink copy and paste fixes"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:339
msgid "Avoid crash when Templates folder is null"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:340
msgid "Reduce memory footprint of FileChooserDialog"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:341
msgid "Avoid crash when selecting image file in some situations"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:342
msgid ""
"Fix unclosable progress window while copying network files to removable "
"storage"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:343
msgid "Fix drag and drop after double clicking a blank area"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:344
msgid "Avoid crash while scrolling over view switcher"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:345
msgid "Avoid possible crash in some copy and paste situations"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:353
msgid "Keyboard navigation fix for cherry picking select files in Grid view"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:354
msgid "Don't hardcode search placeholder text style, fixes dark theme issue"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:362
msgid "Minor spacing adjustments to location bar"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:363
msgid "Fix F2 renaming behavior"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:371
msgid "Minor spacing adjustments to location breadcrumbs"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:372
msgid "Consistently remember color tags"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:373
msgid ""
"Reload thumbnails when changing zoom level to avoid showing placeholder "
"unnecessarily"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:374
msgid "Fix running scripts with spaces in filename"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:382
msgid "Fix high CPU regression in version 4.1.4"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:383
msgid "File sorting fixes"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:391
msgid "Meson: fix soversion names"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:392
msgid "Remove CMake build system"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:393
msgid "Fix missing File System properties menu"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:394
msgid "Drop intltool dependency"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:395
msgid "Fix default file type handler logic"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:396
msgid "Fix initial search window size too small"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:397
msgid "Add option to disable local file thumbnails"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:398
msgid "Fix crash when opening multiple video files"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:399
msgid "Fix some timing issues around adding and removing files"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:400
msgid "Meson: fix missing library headers"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:401
msgid "Fix color tag display"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:402
msgid "Show reserved space in Properties window"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:410
msgid "Add border radius to text background"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:411
msgid "Fix file mimetype association regression"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:412
msgid "New pathbar tooltips"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:413
msgid "Build fixes"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:421
msgid "Plugin build fixes"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:429
msgid "Fix Ctrl+Tab behavior"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:430
msgid "Fix building without Unity library"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:437
msgid "Hide breadcrumbs and show placeholder and search icon in home folder"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:438
msgid "Style error dialogs"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:439
msgid "Right click fixes"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:440
msgid "Reload recent view when privacy setting changes"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:441
msgid "`Connect Server` fixes"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:442
msgid "Do not activate multiple files with single click"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:443
msgid "Use `Tab` to toggle View and Sidebar keyboard focus"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:444
msgid "Delete color tag database entries for trashed files"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:445
msgid "Throttle tab closing"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:446
msgid "`Ctrl` key fixes"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:447
msgid "Drag and drop fixes"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:448
msgid "Show icons in `Open with` menus"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:449
msgid "Tooltip fixes"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:450
msgid "Memory leak fixes"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:451
msgid "Crash fixes"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:452
msgid "Trash: respect sound setting"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:453
msgid "Localization fixes"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:461
msgid "Over 100 major and minor bug fixes and improvements"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:468
msgid "Honor 12/24hr system setting"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:469
msgid "Distinguish between tabs with the same name"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:470
msgid "Support launching from other applications with a target file selected"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:471
#: data/io.elementary.files.appdata.xml.in.in:484
#: data/io.elementary.files.appdata.xml.in.in:498
#: data/io.elementary.files.appdata.xml.in.in:508
#: data/io.elementary.files.appdata.xml.in.in:519
msgid "New translations"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:472
#: data/io.elementary.files.appdata.xml.in.in:485
#: data/io.elementary.files.appdata.xml.in.in:499
#: data/io.elementary.files.appdata.xml.in.in:509
#: data/io.elementary.files.appdata.xml.in.in:520
#: data/io.elementary.files.appdata.xml.in.in:527
#: data/io.elementary.files.appdata.xml.in.in:534
#: data/io.elementary.files.appdata.xml.in.in:558
msgid "Minor bug fixes"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:479
msgid "Improved networking support"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:480
msgid "Correct window geometry when snapping to left or right"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:481
msgid ""
"Pressing Ctrl no longer cancels renaming while the \"Reveal Pointer\" "
"setting is active"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:482
msgid "Switching input language cancels actions"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:483
#: data/io.elementary.files.appdata.xml.in.in:497
#: data/io.elementary.files.appdata.xml.in.in:507
#: data/io.elementary.files.appdata.xml.in.in:518
msgid "Various crash fixes"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:492
msgid ""
"Web browsers like Firefox now remember the most recently used downloads "
"directory"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:493
msgid "Remember preferred zoom level"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:494
msgid "Improved input method support"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:495
msgid "910x640 minimum window size"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:496
msgid "Security fixes"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:506
msgid "Enable drag and drop tabs between windows"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:516
msgid "Merge search functionality into Ctrl+F"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:517
msgid "Improved translation support"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:541
msgid "Fix appdata release dates"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:548
msgid "Improve file opening over Samba shares"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:549
msgid "Fix a crash when restoring items from the Trash"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:550
msgid "Improve cut/copy/paste sensitivity in context menu"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:551
msgid "Translation updates"
msgstr ""

#: data/io.elementary.files.appdata.xml.in.in:563
msgid "elementary, Inc."
msgstr ""

#: data/io.elementary.files.policy.in.in:10
msgid "Run Pantheon Files as Administrator"
msgstr ""

#: data/io.elementary.files.policy.in.in:11
msgid "Authentication is required to run Files as Administrator"
msgstr ""
